import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const HTTP_POST = "post";
const HTTP_PUT = "put";

@Injectable({
  providedIn: 'root'
})
export class CrytoService {

  constructor(public http: HttpClient) { }

  private gvuser_key: string = "abc";

  // utils to convert an ArrayBuffer to Hexadexicmal
  private abuf2hex(buf) {
    return Array.prototype.map.call(new Uint8Array(buf), x => ('00' + x.toString(16)).slice(-2)).join('');
  }

  // generate sha256 digest from input string and return it as hexadecimal
  public hash_sha256(str: string) {
    return new Promise(
      (resolve, reject) => {
        const encoder = new TextEncoder();
        const decoder = new TextDecoder();
        const data = encoder.encode(str);
        window.crypto.subtle.digest('SHA-256', data).then(
          (digest) => {
            resolve(this.abuf2hex(digest));
          }
        );
      });
  }

  /*
     * function to be used in order to use authentificated
     * http request: token + gvuser.
     *
     * The observed is embedded in a promise, use .then() when called.
     *
     * @param route  string    The target route
     * @param method string    The http method
     */
  public http_auth_req(route, method, body) {

    let user = window.localStorage.getItem('usuario');
    let token = window.localStorage.getItem('token');

    return new Promise(
      (resolve, reject) => {

        // generate gvuser using sha256
        this.hash_sha256(this.gvuser_key.concat(user)).then(
          (gvuser) => {

            let h = new HttpHeaders(
              {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json',
                'Gv-User': '' + gvuser,
              }
            );

            if (method === HTTP_PUT || method === HTTP_POST) {
              // send the asked method with the body to the given route
              this.http[method](route,
                JSON.stringify(body),
                { headers: h }).toPromise().then(
                  (res) => {
                    resolve(res)
                  },
                  (err) => {
                    reject(err)
                  });
            } else {
              this.http[method](route,
                { headers: h }).toPromise().then(
                  (res) => {
                    resolve(res)
                  },
                  (err) => {
                    reject(err)
                  });
            }
          });
      }
    );
  }

}

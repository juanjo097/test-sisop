import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { CrytoService } from '../../cryto.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  constructor(
    public API: ApiService,
    public crypto: CrytoService
  ) { }

  ngOnInit()
  {
  }


  submitFormLogin(user: string, password: string)
  {
    if (!user || !password)
    {
      window.alert('Password or user invalid');
    }
    else
    {

      this.crypto.hash_sha256(password).then(
        (digest) =>
        {
          this.API.login(user, digest).subscribe(
            (data)=>
            {
              localStorage.setItem('user', user);
              localStorage.setItem('token', data['respuesta']);
            },
            (err)=>
            {

            }
          );
        },
        (err) =>
        {

        }
      );

    }
  }


}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CrytoService } from './cryto.service';

const HTTP_POST = "post";
const HTTP_PUT = "put";
const HTTP_GET = "get";
const HTTP_DELETE = "delete";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  domain: string = 'http://192.168.15.38/sisop_backend/index.php/';

  noauth_header: HttpHeaders;

  constructor(
    public http: HttpClient,
    public crypto: CrytoService
  ) {

    this.noauth_header = new HttpHeaders(
      {
        'Content-Type': 'application/json',
      }
    );

  }

  /*
  * @params :
  * user : string,
  * password: hashed pass
  * login function
  */
  public login(user, password)
  {
    let body = {
	    'name': user,
	    'password': password,
	};
  console.log(body);

	// not authentificated request, classic method
	return this.http.post(this.domain + "user/login", JSON.stringify(body), { headers: this.noauth_header });
  }

}
